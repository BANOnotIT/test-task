/// <reference types="react-scripts" />

declare module 'just-debounce-it' {
    export default function <F>(fn: F, delay: number, immediate?: boolean): F
}
declare module '*.module.sass' {
    interface IClassNames {
        [className: string]: string
    }
    const classNames: IClassNames
    export = classNames
}

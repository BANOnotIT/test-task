import debounce from 'just-debounce-it'
import { observer } from 'mobx-react'
import React, { SyntheticEvent, useCallback, useEffect, useMemo, useState } from 'react'

import SearchService from '../services/SearchService'
import { useService } from '../utils'
import styles from './Header.module.sass'

export default observer(() => {
    const searchService = useService(SearchService)
    const searchEpisode = searchService.searchEpisode
    const lookupEpisode = useMemo(
        () =>
            debounce((e: string) => {
                const episode = parseInt(e, 10)
                searchService.lookup(isNaN(episode) ? null : episode)
            }, 200),
        [searchService]
    )

    const [search, setSearch] = useState('')
    const onChange = useCallback(
        (e: SyntheticEvent) => {
            const value = (e.target as HTMLInputElement).value.replace(/[^\s\d]+/gi, '')
            setSearch(value)
            lookupEpisode(value)
        },
        [lookupEpisode]
    )

    useEffect(() => {
        let newSearch = searchEpisode?.toString() ?? ''
        setSearch(newSearch)
    }, [searchEpisode])

    return (
        <div className={styles.cover}>
            <header className={styles.container}>
                <div className={styles.logo}>
                    <h1 className={styles.title}>Rick and Morty</h1>
                    <span>База данных героев</span>
                </div>

                <input
                    className={styles.searchField}
                    type="text"
                    placeholder="Введите номер серии"
                    onChange={onChange}
                    value={search}
                    pattern="\d*"
                />
            </header>
        </div>
    )
})

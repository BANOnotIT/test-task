import { Container } from 'inversify'
import { interfaces } from 'inversify/dts/interfaces/interfaces'
import { useContext } from 'react'

import { ContainerContext } from './contexts/ContainerContext'
import { Charter } from './types/external-api'

export function useService<T>(serviceIdentifier: interfaces.ServiceIdentifier<T>): T {
    const container: Container = useContext(ContainerContext)
    return container.get(serviceIdentifier)
}

export function getIdFromCharter(charter: Charter): string {
    const namePart = charter.name.toLowerCase().replace(/\s+/g, '')
    return `${namePart}ID${charter.id.toString()}`
}

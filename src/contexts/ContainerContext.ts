import { createContext } from 'react'

import { container } from '../inversify.config'

export const ContainerContext = createContext(container)

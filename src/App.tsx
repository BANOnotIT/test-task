import { observer } from 'mobx-react'
import React from 'react'

import styles from './App.module.sass'
import Header from './containers/Header'
import CharterList from './pages/ChartersList'

function App() {
    return (
        <div className={styles.app}>
            <Header />
            <main className={styles.content}>
                <CharterList />
            </main>
        </div>
    )
}

export default observer(App)

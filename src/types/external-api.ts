import { UrlString, UTCString } from './util'

export enum CharterStatus {
    alive = 'Alive',
    dead = 'Dead',
    unknown = 'unknown',
}

export enum Gender {
    fem = 'Female',
    male = 'Male',
    genderless = 'Genderless',
    unknown = 'unknown',
}

export type Charter = {
    id: number
    name: string
    status: CharterStatus
    species: string
    type: string
    gender: Gender
    origin: {
        name: string
        url: UrlString
    }
    location: {
        name: string
        url: UrlString
    }
    image: string
    episode: UrlString[]
    url: UrlString
    created: UTCString
}

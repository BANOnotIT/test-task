import 'reflect-metadata'
import './index.css'

import React from 'react'
import ReactDOM from 'react-dom'

import App from './App'
import { ContainerContext } from './contexts/ContainerContext'
import { container } from './inversify.config'

ReactDOM.render(
    <React.StrictMode>
        <ContainerContext.Provider value={container}>
            <App />
        </ContainerContext.Provider>
    </React.StrictMode>,
    document.getElementById('root')
)

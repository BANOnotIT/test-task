import { observer } from 'mobx-react'
import React, { useEffect } from 'react'

import Card from '../components/Card'
import { ChartersService } from '../services/ChartersService'
import SearchService from '../services/SearchService'
import { useService } from '../utils'
import styles from './ChartersList.module.sass'

const CharterList = observer(() => {
    const searchService = useService(SearchService)
    const charterService = useService(ChartersService)

    useEffect(() => {
        // noinspection JSIgnoredPromiseFromCall
        charterService.getCharters()
    })

    if (charterService.isLoading) return <div>Загрузка вселенной BXS-71...</div>

    return searchService.items.length !== 0 ? (
        <div>
            <p className={[styles.big, styles.message].join(' ')}>В базе вселенной удалось найти:</p>
            <div className={styles.grid}>
                {searchService.items.map((charter) => (
                    <Card key={charter.id} charter={charter} />
                ))}
            </div>
        </div>
    ) : (
        <div className={styles.message}>
            <p className={styles.big}>Ничего не найдено</p>
            <p>Возможно, в далёких уголках вселенной есть ответ, но не в этой маленькой части данных</p>
        </div>
    )
})

export default CharterList

import * as React from 'react'

import { Charter, CharterStatus } from '../types/external-api'
import { getIdFromCharter } from '../utils'
import * as styles from './Card.module.sass'

type Props = {
    charter: Charter
}
export default ({ charter }: Props) => {
    const statusClass =
        charter.status === CharterStatus.alive
            ? styles.statusAlive
            : charter.status === CharterStatus.dead
            ? styles.statusDead
            : styles.status

    return (
        <section className={styles.container}>
            <div className={styles.avatarContainer}>
                <img className={styles.avatar} src={charter.image} alt={`${charter.name} avatar`} />
                <span className={statusClass}>{charter.status}</span>
            </div>
            <div className={styles.dataContainer}>
                <p className={styles.id}>{getIdFromCharter(charter)}</p>
                <h3 className={styles.name}>{charter.name}</h3>
                <p>Вид: {charter.species}</p>
                <p>Гендер: {charter.gender}</p>
                <p>Происхождение: {charter.origin.name}</p>
                <p>Местоположение: {charter.location.name}</p>
            </div>
        </section>
    )
}

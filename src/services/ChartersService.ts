import { injectable } from 'inversify'
import { action, computed, makeAutoObservable, runInAction } from 'mobx'

import { Charter } from '../types/external-api'

@injectable()
export class ChartersService {
    public isLoading = false
    public error: any | null = null
    private cachedCharters: Charter[] = []

    constructor() {
        makeAutoObservable(this)
    }

    @computed
    public get charters(): Charter[] {
        return this.cachedCharters
    }

    private get cacheValid() {
        return this.cachedCharters.length !== 0
    }

    @action
    async getCharters(): Promise<Charter[]> {
        if (this.cacheValid) return this.cachedCharters

        this.error = null
        this.isLoading = true

        const response = await fetch('https://rickandmortyapi.com/api/character/')
        const data = await response.json()

        if (response.status === 200) {
            // here should be some validation
            const newCharters = data.results as Charter[]

            return runInAction(() => {
                this.cachedCharters = newCharters
                this.isLoading = false
                this.error = null
                return this.charters
            })
        } else {
            return runInAction(() => {
                this.isLoading = false
                this.error = data
                return this.charters
            })
        }
    }
}
